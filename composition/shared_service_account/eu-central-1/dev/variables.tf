## PROVIDER ##
variable "aws_region" {}
variable "account_id" {}

## Jenkins EC2 ## 
variable "public_key" {}
variable "key_name" {}
variable "vpc_id" {}
variable "iam_instance_profile_name" {}
variable "ami_id" {}

##  Common tag metadata ## 
variable "env" {}
variable "app_name" {}
variable "owner" {}
variable "scope" {}