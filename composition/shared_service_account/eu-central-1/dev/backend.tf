terraform {
  backend "s3" {
    bucket = "terraform-jenkins-state-hisashi"
    key    = "dev"
    region = "eu-central-1"
  }
}
