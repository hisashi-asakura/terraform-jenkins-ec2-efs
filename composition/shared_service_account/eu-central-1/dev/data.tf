locals {
  subnet_id                     = "${element(data.aws_subnet_ids.this.ids, 0)}"
  efs_mount_target_subnet_ids   = "${data.aws_subnet_ids.this.ids}"
  efs_mount_target_subnet_count = "${length(data.aws_subnet_ids.this.ids)}"
}

data "aws_subnet_ids" "this" {
  vpc_id = "${var.vpc_id}"
}
