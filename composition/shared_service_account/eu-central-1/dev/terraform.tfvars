## Provider ##
aws_region = "eu-central-1"
account_id = "912233601461"

## Jenkins EC2 ## 
public_key = "test.pub"
key_name = "terraform-jenkins"
vpc_id = "vpc-0be8b12f435fcb680"
iam_instance_profile_name = "JenkinsTerraformRole"
ami_id = "ami-0dadb60ec5455dda7"

## Metadata ##
env = "dev"
app_name = "jenkins"
owner = ""
scope = ""