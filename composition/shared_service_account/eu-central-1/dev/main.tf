module "jenkins" {
  source = "../../../../infrastructure_module/jenkins_ec2"

  ## Jenkins EC2 ##
  public_key           = "${var.public_key}"
  key_name             = "${var.key_name}"
  vpc_id               = "${var.vpc_id}"
  subnet_id            = "${local.subnet_id}"
  iam_instance_profile = "${var.iam_instance_profile_name}"
  ami_id               = "${var.ami_id}"

  ## Jenkins EFS ## 
  efs_mount_target_subnet_count = "${local.efs_mount_target_subnet_count}"
  efs_mount_target_subnet_ids   = "${local.efs_mount_target_subnet_ids}"

  ## Common tag metadata ##
  env      = "${var.env}"
  app_name = "${var.app_name}"
  owner    = "${var.owner}"
  scope    = "${var.scope}"

  aws_region = "${var.aws_region}"
}
