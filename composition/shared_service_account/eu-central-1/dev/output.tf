########################################
# JENKINS
########################################
## EC2 ##
output "jenkins_ec2_id" {
  value = "${module.jenkins.ec2_id}"
}

output "jenkins_ec2_arn" {
  value = "${module.jenkins.ec2_arn}"
}

output "jenkins_ec2_availability_zone" {
  value = "${module.jenkins.ec2_availability_zone}"
}

output "jenkins_ec2_placement_group" {
  value = "${module.jenkins.ec2_placement_group}"
}

output "jenkins_ec2_key_name" {
  value = "${module.jenkins.ec2_key_name}"
}

output "jenkins_ec2_password_data" {
  value       = "${module.jenkins.ec2_password_data}"
  description = "Base-64 encoded encrypted password data for the instance. Useful for getting the administrator password for instances running Microsoft Windows. attribute is only exported if get_password_data is true. Note that encrypted value will be stored in the state file, as with all exported attributes. See GetPasswordData for more information."
}

output "jenkins_ec2_public_dns" {
  value = "${module.jenkins.ec2_public_dns}"
}

output "jenkins_ec2_public_ip" {
  value = "${module.jenkins.ec2_public_ip}"
}

output "jenkins_ec2_ipv6_addresses" {
  value = "${module.jenkins.ec2_ipv6_addresses}"
}

output "jenkins_ec2_primary_network_interface_id" {
  value = "${module.jenkins.ec2_primary_network_interface_id}"
}

output "jenkins_ec2_private_dns" {
  value = "${module.jenkins.ec2_private_dns}"
}

output "jenkins_ec2_private_ip" {
  value = "${module.jenkins.ec2_private_ip}"
}

output "jenkins_ec2_security_groups" {
  value = "${module.jenkins.ec2_security_groups}"
}

output "jenkins_ec2_vpc_security_group_ids" {
  value = "${module.jenkins.ec2_vpc_security_group_ids}"
}

output "jenkins_ec2_subnet_id" {
  value = "${module.jenkins.ec2_subnet_id}"
}

## EFS ##
output "jenkins_efs_id" {
  value = "${module.jenkins.jenkins_efs_id}"
}

output "jenkins_efs_arn" {
  value = "${module.jenkins.jenkins_efs_arn}"
}

output "jenkins_efs_dns_name" {
  value = "${module.jenkins.jenkins_efs_dns_name}"
}

output "jenkins_efs_mount_target_id" {
  value = "${module.jenkins.jenkins_efs_mount_target_id}"
}

output "jenkins_efs_mount_target_dns_name" {
  value = "${module.jenkins.jenkins_efs_mount_target_dns_name}"
}

output "jenkins_efs_mount_target_network_interface_id" {
  value = "${module.jenkins.jenkins_efs_mount_target_network_interface_id}"
}

output "jenkins_efs_mount_target_security_group_id" {
  value = "${module.jenkins.jenkins_efs_mount_target_security_group_id}"
}

output "jenkins_efs_mount_target_security_group_arn" {
  value = "${module.jenkins.jenkins_efs_mount_target_security_group_arn}"
}

output "jenkins_efs_mount_target_security_group_vpc_id" {
  value = "${module.jenkins.jenkins_efs_mount_target_security_group_vpc_id}"
}

output "jenkins_efs_mount_target_security_group_name" {
  value = "${module.jenkins.jenkins_efs_mount_target_security_group_name}"
}

output "jenkins_efs_mount_target_security_group_ingress" {
  value = "${module.jenkins.jenkins_efs_mount_target_security_group_ingress}"
}

output "jenkins_efs_mount_target_security_group_egress" {
  value = "${module.jenkins.jenkins_efs_mount_target_security_group_egress}"
}
