module "test_vpn_connection" {
  source = "../../../../infrastructure_module/test_vpn"

  ## Common tag metadata ##
  env      = "${var.env}"
  app_name = "${var.app_name}"
  owner    = "${var.owner}"
  scope    = "${var.scope}"

  aws_region = "${var.aws_region}"
}