pipeline {
    agent none

    triggers {
        bitbucketPush()
    }

    environment {
        BITBUCKET_CRED_ID = "cr_bitbucket_hisashi"
        BITBUCKET_BRANCH = "master"
        BITBUCKET_REPO_URL = "https://bitbucket.org/hisashi-asakura/terraform-jenkins-ec2-efs/"
        
        TERRAFORM_SCRIPT_PATH = "composition/shared_service_account/eu-central-1/dev"
        TERRAFORM_DOCKER_RUN_CMD = 'docker run \
                                      --workdir /terraform/composition/shared_service_account/eu-central-1/dev \
                                      -v ${WORKSPACE}:/terraform \
                                      hashicorp/terraform:light'
        
        AWS_REGION = "eu-central-1"
        AWS_ACCOUT_ID = "912233601461"
        AMI_ID = "ami-0dadb60ec5455dda7"
        EC2_SSH_KEY_NAME = "terraform-jenkins"
        EC2_SSH_KEY_PASSWORD = ""
        IAM_INSTANCE_PROFILE_NAME = "JenkinsTerraformRole"
        VPC_ID = "vpc-0be8b12f435fcb680"
    }

    stages {
        stage("Clean workspace and checkout SCM") {
            agent any 
            steps {
                cleanWs()

                git url: BITBUCKET_REPO_URL, 
                    branch: BITBUCKET_BRANCH,
                    credentialsId: BITBUCKET_CRED_ID

                sh """
                  ls -al
                  cd ${TERRAFORM_SCRIPT_PATH}
                  ls -al
                """
            }
        }

        stage("Generate a ssh key for EC2") {
            agent any 
            steps {
                dir("${TERRAFORM_SCRIPT_PATH}") {
                    sh """
                        ssh-keygen -P "" -f ${EC2_SSH_KEY_NAME} 
                    """
                }
            }
        }

        stage("Replace tfvar's values") {
            agent any 
            steps {
                replaceVariables()
            }
        }

        stage("Terraform Init") {
            agent any 
            
            steps {
                terraform("init")
            }
        }
        
        stage("Terraform Plan") {
            agent any 
            steps {
                terraform("plan")
                
            }
        }
        
        stage("Approve Terraform Plan") {
            agent any 
            steps {
                terraform("approve")
            }
        }

        stage("Terraform Apply") {
            agent any 
            steps {
                terraform("apply")
            }
        }

    }
}

def replaceVariables() {
    dir("${TERRAFORM_SCRIPT_PATH}") {
        sh """
            sed -i 's|^aws_region = .*|aws_region = "'"${AWS_REGION}"'"|
                    s|^account_id = .*|account_id = "'"${AWS_ACCOUT_ID}"'"|
                    s|^public_key = .*|public_key = "'"${EC2_SSH_KEY_NAME}.pub"'"|
                    s|^key_name = .*|key_name = "'"${EC2_SSH_KEY_NAME}"'"|
                    s|^vpc_id = .*|vpc_id = "'"${VPC_ID}"'"|
                    s|^iam_instance_profile_name = .*|iam_instance_profile_name = "'"${IAM_INSTANCE_PROFILE_NAME}"'"|
                    s|^ami_id = .*|ami_id = "'"${AMI_ID}"'"|' terraform.tfvars
        """
    }
}

def terraform(command) {
    withAWS(region: "${AWS_REGION}") {
        if (command == "init") {
            sh """
                ${TERRAFORM_DOCKER_RUN_CMD} init
            """
        }
        else if (command == "plan") {
            sh """
                ${TERRAFORM_DOCKER_RUN_CMD} plan -out=tfplan
            """
        }
        else if (command == "apply") {
            sh """
                ${TERRAFORM_DOCKER_RUN_CMD} apply tfplan
            """
        }
        else if (command == "approve") {
            input "Do you want to apply Terraform?"
        }
    }
}