output "id" {
  value = "${aws_customer_gateway.this.id}"
}

output "bgp_asn" {
  value = "${aws_customer_gateway.this.bgp_asn}"
}

output "ip_address" {
  value = "${aws_customer_gateway.this.ip_address}"
}

output "type" {
  value = "${aws_customer_gateway.this.type}"
}

output "tags" {
  value = "${aws_customer_gateway.this.tags}"
}