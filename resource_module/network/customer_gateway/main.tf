resource "aws_customer_gateway" "this" {
  bgp_asn    = "${var.customer_gateway_bgp_asn}"
  ip_address = "${var.customer_gateway_ip_address}"
  type       = "${var.customer_gateway_type}"

  tags = "${var.customer_gateway_tags}"
}