
## Customer Gateway
variable "customer_gateway_bgp_asn" {}
variable "customer_gateway_ip_address" {}
variable "customer_gateway_type" {}
variable "customer_gateway_tags" { type = "map" }