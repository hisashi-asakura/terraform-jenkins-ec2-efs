resource "aws_efs_file_system" "this" {
  creation_token = "terraform-efs"
  encrypted      = "${var.efs_encrypted}"

  tags = "${var.efs_tags}"
}

resource "aws_efs_mount_target" "this" {
  count           = "${length(var.efs_mount_target_subnet_ids)}"
  file_system_id  = "${aws_efs_file_system.this.id}"
  subnet_id       = "${var.efs_mount_target_subnet_ids[count.index]}"
  security_groups = ["${aws_security_group.this.id}"]
}

resource "aws_security_group" "this" {
  name        = "${var.efs_security_group_name}"
  description = "${var.efs_security_group_description}"
  vpc_id      = "${var.efs_security_group_vpc_id}"

  ingress {
    from_port       = "${var.efs_security_group_ingress_from_port}"
    to_port         = "${var.efs_security_group_ingress_to_port}"
    protocol        = "${var.efs_security_group_ingress_protocol}"
    security_groups = ["${var.efs_security_group_ingress_security_groups}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${var.efs_security_group_tags}"
}
