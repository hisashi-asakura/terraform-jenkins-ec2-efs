## EFS FILE SYSTEM ## 
variable "efs_encrypted" {}
variable "efs_tags" {
  type = "map"
}

## EFS MOUNT TARGET ## 
variable "count" {}
variable "efs_mount_target_subnet_ids" {
  type = "list"
}

## EFS SECURITY GROUP ## 
variable "efs_security_group_name" {}
variable "efs_security_group_description" {}
variable "efs_security_group_vpc_id" {}
variable "efs_security_group_ingress_from_port" {}
variable "efs_security_group_ingress_to_port" {}
variable "efs_security_group_ingress_protocol" {}
variable "efs_security_group_ingress_security_groups" {}
variable "efs_security_group_tags" {
  type = "map"
}
