########################################
# EFS FILE SYSTEM
########################################
output "efs_id" {
  value = "${aws_efs_file_system.this.id}"
}

output "efs_arn" {
  value = "${aws_efs_file_system.this.arn}"
}

output "efs_dns_name" {
  value = "${aws_efs_file_system.this.dns_name}"
}

########################################
# EFS MOUNT TARGET
########################################
output "efs_mount_target_id" {
  value = "${aws_efs_mount_target.this.*.id}"
}

output "efs_mount_target_dns_name" {
  value = "${aws_efs_mount_target.this.*.dns_name}"
}

output "efs_mount_target_network_interface_id" {
  value = "${aws_efs_mount_target.this.*.network_interface_id}"
}

########################################
# EFS MOUNT TARGET SG
########################################
output "efs_mount_target_security_group_id" {
  value = "${aws_security_group.this.id}"
}

output "efs_mount_target_security_group_arn" {
  value = "${aws_security_group.this.arn}"
}

output "efs_mount_target_security_group_vpc_id" {
  value = "${aws_security_group.this.vpc_id}"
}

output "efs_mount_target_security_group_name" {
  value = "${aws_security_group.this.name}"
}

output "efs_mount_target_security_group_ingress" {
  value = "${aws_security_group.this.ingress}"
}

output "efs_mount_target_security_group_egress" {
  value = "${aws_security_group.this.egress}"
}
