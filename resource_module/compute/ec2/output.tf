########################################
# AWS INSTANCE
########################################
output "id" {
  value       = "${aws_instance.this.id}"
  description = "The instance ID."
}

output "arn" {
  value       = "${aws_instance.this.arn}"
  description = "The ARN of the instance."
}

output "availability_zone" {
  value       = "${aws_instance.this.availability_zone}"
  description = "The availability zone of the instance."
}

output "placement_group" {
  value       = "${aws_instance.this.placement_group}"
  description = "The placement group of the instance."
}

output "key_name" {
  value       = "${aws_instance.this.key_name}"
  description = "The key name of the instance"
}

output "password_data" {
  value       = "${aws_instance.this.password_data}"
  description = "Base-64 encoded encrypted password data for the instance. Useful for getting the administrator password for instances running Microsoft Windows. This attribute is only exported if get_password_data is true. Note that this encrypted value will be stored in the state file, as with all exported attributes. See GetPasswordData for more information."
}

output "public_dns" {
  value       = "${aws_instance.this.public_dns}"
  description = "The public DNS name assigned to the instance. For EC2-VPC, this is only available if you've enabled DNS hostnames for your VPC"
}

output "public_ip" {
  value       = "${aws_instance.this.public_ip}"
  description = "The public IP address assigned to the instance, if applicable. NOTE: If you are using an aws_eip with your instance, you should refer to the EIP's address directly and not use public_ip, as this field will change after the EIP is attached."
}

output "ipv6_addresses" {
  value       = "${aws_instance.this.ipv6_addresses}"
  description = "A list of assigned IPv6 addresses, if any"
}

output "primary_network_interface_id" {
  value       = "${aws_instance.this.primary_network_interface_id}"
  description = "The ID of the instance's primary network interface."
}

output "private_dns" {
  value       = "${aws_instance.this.private_dns}"
  description = "The private DNS name assigned to the instance. Can only be used inside the Amazon EC2, and only available if you've enabled DNS hostnames for your VPC"
}

output "private_ip" {
  value       = "${aws_instance.this.private_ip}"
  description = "The private IP address assigned to the instance"
}

output "security_groups" {
  value       = "${aws_instance.this.security_groups}"
  description = "The associated security groups."
}

output "vpc_security_group_ids" {
  value       = "${aws_instance.this.vpc_security_group_ids}"
  description = "The associated security groups in non-default VPC"
}

output "subnet_id" {
  value       = "${aws_instance.this.subnet_id}"
  description = "The VPC subnet ID."
}
