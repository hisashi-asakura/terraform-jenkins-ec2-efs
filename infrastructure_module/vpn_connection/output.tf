## VPN ## 
output "vpn_connection_id" {
  value = "${module.vpn_gateway.vpn_connection_id}"
}

output "vpn_connection_tunnel1_address" {
  value = "${module.vpn_gateway.vpn_connection_tunnel1_address}"
}

output "vpn_connection_tunnel1_cgw_inside_address" {
  value = "${module.vpn_gateway.vpn_connection_tunnel1_cgw_inside_address}"
}

output "vpn_connection_tunnel1_vgw_inside_address" {
  value = "${module.vpn_gateway.vpn_connection_tunnel1_vgw_inside_address}"
}

output "vpn_connection_tunnel2_address" {
  value = "${module.vpn_gateway.vpn_connection_tunnel2_address}"
}

output "vpn_connection_tunnel2_cgw_inside_address" {
  value = "${module.vpn_gateway.vpn_connection_tunnel2_cgw_inside_address}"
}

output "vpn_connection_tunnel2_vgw_inside_address" {
  value = "${module.vpn_gateway.vpn_connection_tunnel2_vgw_inside_address}"
}

## Customer Gateway ## 
output "customer_gateway_id" {
  value = "${module.customer_gateway.id}"
}

output "customer_gateway_bgp_asn" {
  value = "${module.customer_gateway.bgp_asn}"
}

output "customer_gateway_ip_address" {
  value = "${module.customer_gateway.ip_address}"
}

output "customer_gateway_type" {
  value = "${module.customer_gateway.type}"
}

output "customer_gateway_tags" {
  value = "${module.customer_gateway.tags}"
}