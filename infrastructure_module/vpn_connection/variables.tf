## Metatada ##
variable "env" {}
variable "app_name" {}
variable "owner" {}
variable "scope" {}
variable "aws_region" {}

## COMMON TAGS ## 
variable "region_tag" {
  type = "map"

  default = {
    "us-east-1"    = "ue1"
    "us-west-1"    = "uw1"
    "eu-west-1"    = "ew1"
    "eu-central-1" = "ec1"
  }
}

variable "environment_tag" {
  type = "map"

  default = {
    "prod"     = "p"
    "pre_prod" = "pp"
    "qa"       = "q"
    "staging"  = "s"
    "dev"      = "d"
  }
}