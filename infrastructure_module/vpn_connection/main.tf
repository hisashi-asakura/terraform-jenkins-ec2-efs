module "vpn_gateway" {
  source = "../../resource_module/network/vpn"

  # VPN
  create_vpn_connection = "${local.create_vpn_connection}"

  vpn_gateway_id      = "${module.vpc.vgw_id}"
  customer_gateway_id = "${module.customer_gateway.id}"

  vpc_id                       = "${module.vpc.vpc_id}"
  vpc_subnet_route_table_ids   = ["${module.vpc.private_route_table_ids}"]
  vpc_subnet_route_table_count = "${length(local.vpc_private_subnets)}"

  # static routes
  vpn_connection_static_routes_only         = "${local.vpn_connection_static_routes_only}"
  vpn_connection_static_routes_destinations = "${local.vpn_connection_static_routes_destinations}"

  # tunnel inside cidr & preshared keys (optional)
  tunnel1_inside_cidr   = "${local.tunnel1_inside_cidr}"
  tunnel2_inside_cidr   = "${local.tunnel2_inside_cidr}"
  tunnel1_preshared_key = "${local.tunnel1_preshared_key}"
  tunnel2_preshared_key = "${local.tunnel2_preshared_key}"
}

module "customer_gateway" {
  source = "../../resource_module/network/customer_gateway"

  customer_gateway_bgp_asn = "${local.customer_gateway_bgp_asn}"
  customer_gateway_ip_address = "${local.customer_gateway_ip_address}"
  customer_gateway_type = "${local.customer_gateway_type}"
  customer_gateway_tags = "${local.customer_gateway_tags}"
}

module "vpc" {
  source = "../../resource_module/network/vpc"

  name = "${local.vpc_name}" 
  cidr = "${local.vpc_cidr}" 
  azs             = "${local.vpc_azs}" 
  private_subnets = ["${local.vpc_private_subnets}"]
  public_subnets  = ["${local.vpc_public_subnets}"]
  enable_vpn_gateway = "${local.vpc_enable_vpn_gateway}"
  tags = "${local.vpc_tags}"
}