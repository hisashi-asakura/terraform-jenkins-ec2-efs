locals {
  ## VPN Gateway ##
  create_vpn_connection = true
  vpn_connection_static_routes_only         = true
  vpn_connection_static_routes_destinations = ["10.100.0.1/32", "10.200.0.1/32"]
  tunnel1_inside_cidr   = "169.254.33.88/30"
  tunnel2_inside_cidr   = "169.254.33.100/30"
  tunnel1_preshared_key = "1234567890abcdefghijklmn"
  tunnel2_preshared_key = "abcdefghijklmn1234567890"


  ## Customer Gateway ##
  customer_gateway_bgp_asn    = 65000
  customer_gateway_ip_address = "172.83.124.12"
  customer_gateway_type       = "ipsec.1"
  customer_gateway_tags = {
    Name = "cgw-${var.region_tag[var.aws_region]}-${var.app_name}-${var.environment_tag[var.env]}"
    Environment = "${var.env}"
    Application = "${var.app_name}"
    Owner       = "${var.owner}"
    Scope       = "${var.scope}"
  }

  ## VPC ## 
  vpc_name = "vpc-${var.region_tag[var.aws_region]}-${var.app_name}-${var.environment_tag[var.env]}"
  vpc_cidr = "10.10.0.0/16"
  vpc_azs = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
  vpc_private_subnets = ["10.10.1.0/24", "10.10.2.0/24", "10.10.3.0/24"]
  vpc_public_subnets = ["10.10.11.0/24", "10.10.12.0/24", "10.10.13.0/24"]
  vpc_enable_vpn_gateway = true
  vpc_tags = {
    Name = "vpc-${var.region_tag[var.aws_region]}-${var.app_name}-${var.environment_tag[var.env]}"
    Environment = "${var.env}"
    Application = "${var.app_name}"
    Owner       = "${var.owner}"
    Scope       = "${var.scope}"
  }
}