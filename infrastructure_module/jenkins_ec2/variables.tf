## EC2 ## 
variable "subnet_id" {}
variable "iam_instance_profile" {}
variable "ami_id" {}

## SECURITY GROUP ##
variable "vpc_id" {}

## KEY PAIR ##
variable "public_key" {}
variable "key_name" {}

## EFS ##
variable efs_mount_target_subnet_count {}
variable efs_mount_target_subnet_ids {
  type = "list"
}

## Metatada ##
variable "env" {}
variable "app_name" {}
variable "owner" {}
variable "scope" {}
variable "aws_region" {}

## COMMON TAGS ## 
variable "region_tag" {
  type = "map"

  default = {
    "us-east-1"    = "ue1"
    "us-west-1"    = "uw1"
    "eu-west-1"    = "ew1"
    "eu-central-1" = "ec1"
  }
}

variable "environment_tag" {
  type = "map"

  default = {
    "prod"     = "p"
    "pre_prod" = "pp"
    "qa"       = "q"
    "staging"  = "s"
    "dev"      = "d"
  }
}
