locals {
  ## EC2 ##
  ami           = "ami-0555d4829da6622e3"
  instance_type = "t3.large"
  user_data     = "${data.template_file.userdata_script.rendered}"
  monitoring    = true

  associate_public_ip_address = true
  private_ip                  = ""

  ebs_optimized          = false
  volume_tags            = {}
  root_block_device      = []
  ebs_block_device       = []
  ephemeral_block_device = []

  source_dest_check                    = true
  disable_api_termination              = false
  instance_initiated_shutdown_behavior = ""
  tenancy                              = "default"

  tags = {
    Name        = "ec2-${var.region_tag[data.aws_region.current.name]}-${var.app_name}-${var.environment_tag[var.env]}"
    Environment = "${var.env}"
    Application = "${var.app_name}"
    Owner       = "${var.owner}"
    Scope       = "${var.scope}"
  }

  ## CloudWatch Alarm ##
  alarm_name = "ec2-autorecover"
  namespace = "AWS/EC2"
  evaluation_periods = 2
  period = 60
  alarm_description = "This metric auto recovers EC2 instances"
  alarm_actions = ["arn:aws:automate:${var.aws_region}:ec2:recover"]
  statistic = "Minimum"
  comparison_operator = "GreaterThanThreshold"
  threshold = 0
  metric_name = "StatusCheckFailed_System"

  ## SG ##
  security_group_name        = "scg-${var.region_tag[data.aws_region.current.name]}-${var.app_name}-${var.environment_tag[var.env]}"
  security_group_description = "Allow 80/8080/22 from CR Munich office"
  ingress_cidr_block         = "88.217.255.228/32"
  ingress_rules              = ["http-80-tcp", "http-8080-tcp", "ssh-tcp"]

  security_group_tags = {
    Name        = "scg-${var.region_tag[data.aws_region.current.name]}-${var.app_name}-${var.environment_tag[var.env]}"
    Environment = "${var.env}"
    Application = "${var.app_name}"
    Owner       = "${var.owner}"
    Scope       = "${var.scope}"
  }

  ## EFS ##
  efs_encrypted = true

  efs_tags = {
    Name        = "efs-${var.region_tag[data.aws_region.current.name]}-${var.app_name}-${var.environment_tag[var.env]}"
    Environment = "${var.env}"
    Application = "${var.app_name}"
    Owner       = "${var.owner}"
    Scope       = "${var.scope}"
  }

  efs_security_group_name              = "efs-scg-${var.region_tag[data.aws_region.current.name]}-${var.app_name}-${var.environment_tag[var.env]}"
  efs_security_group_description       = "Allow 2049 from EC2 SG"
  efs_security_group_ingress_from_port = "2049"
  efs_security_group_ingress_to_port   = "2049"
  efs_security_group_ingress_protocol  = "tcp"

  efs_security_group_tags = {
    Name        = "efs-scg-${var.region_tag[data.aws_region.current.name]}-${var.app_name}-${var.environment_tag[var.env]}"
    Environment = "${var.env}"
    Application = "${var.app_name}"
    Owner       = "${var.owner}"
    Scope       = "${var.scope}"
  }
}

## EC2 ##
data "template_file" "userdata_script" {
  template = "${file("../../../../infrastructure_module/jenkins_ec2/scripts/userdata_script_efs_jenkins.cfg")}"

  vars {
    efs_dns = "${module.jenkins_efs.efs_dns_name}"
  }
}

data "aws_region" "current" {}
