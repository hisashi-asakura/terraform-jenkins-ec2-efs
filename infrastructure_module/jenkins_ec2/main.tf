module "jenkins_ec2" {
  source = "../../resource_module/compute/ec2"

  ami                    = "${var.ami_id}"
  instance_type          = "${local.instance_type}"
  user_data              = "${local.user_data}"
  subnet_id              = "${var.subnet_id}"
  key_name               = "${aws_key_pair.this.key_name}"
  monitoring             = "${local.monitoring}"
  vpc_security_group_ids = ["${module.security_group.security_group_id}"]
  iam_instance_profile   = "${var.iam_instance_profile}"

  associate_public_ip_address = "${local.associate_public_ip_address}"
  private_ip                  = "${local.private_ip}"

  ebs_optimized          = "${local.ebs_optimized}"
  volume_tags            = "${local.volume_tags}"
  root_block_device      = "${local.root_block_device}"
  ebs_block_device       = "${local.ebs_block_device}"
  ephemeral_block_device = "${local.ephemeral_block_device}"

  source_dest_check                    = "${local.source_dest_check}"
  disable_api_termination              = "${local.disable_api_termination}"
  instance_initiated_shutdown_behavior = "${local.instance_initiated_shutdown_behavior}"
  tenancy                              = "${local.tenancy}"

  tags = "${local.tags}"

  ## CloudWatch Alarm ##
  alarm_name          = "${local.alarm_name}"
  namespace           = "${local.namespace}"
  evaluation_periods  = "${local.evaluation_periods}"
  period              = "${local.period}"
  alarm_description   = "${local.alarm_description}"
  alarm_actions       = "${local.alarm_actions}"
  statistic           = "${local.statistic}"
  comparison_operator = "${local.comparison_operator}"
  threshold           = "${local.threshold}"
  metric_name         = "${local.metric_name}"
}

module "security_group" {
  source = "../../resource_module/compute/security_group"

  name        = "${local.security_group_name}"
  description = "${local.security_group_description}"
  vpc_id      = "${var.vpc_id}"

  ingress_cidr_blocks = ["${local.ingress_cidr_block}"]
  ingress_rules       = "${local.ingress_rules}"
  egress_rules        = ["all-all"]

  tags = "${local.security_group_tags}"
}

resource "aws_key_pair" "this" {
  key_name   = "${var.key_name}"
  public_key = "${file("${var.public_key}")}"
}

module "jenkins_efs" {
  source = "../../resource_module/storage/efs"

  ## EFS FILE SYSTEM ## 
  efs_encrypted = "${local.efs_encrypted}"
  efs_tags      = "${local.efs_tags}"

  ## EFS MOUNT TARGET ## 
  count                       = "${var.efs_mount_target_subnet_count}"
  efs_mount_target_subnet_ids = "${var.efs_mount_target_subnet_ids}"

  ## EFS SECURITY GROUP ## 
  efs_security_group_name                    = "${local.efs_security_group_name}"
  efs_security_group_description             = "${local.efs_security_group_description}"
  efs_security_group_vpc_id                  = "${var.vpc_id}"
  efs_security_group_ingress_from_port       = "${local.efs_security_group_ingress_from_port}"
  efs_security_group_ingress_to_port         = "${local.efs_security_group_ingress_to_port}"
  efs_security_group_ingress_protocol        = "${local.efs_security_group_ingress_protocol}"
  efs_security_group_ingress_security_groups = "${module.security_group.security_group_id}"
  efs_security_group_tags                    = "${local.efs_security_group_tags}"
}
