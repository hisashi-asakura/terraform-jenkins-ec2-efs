########################################
# JENKINS EC2
########################################
output "ec2_id" {
  value = "${module.jenkins_ec2.id}"
}

output "ec2_arn" {
  value = "${module.jenkins_ec2.arn}"
}

output "ec2_availability_zone" {
  value = "${module.jenkins_ec2.availability_zone}"
}

output "ec2_placement_group" {
  value = "${module.jenkins_ec2.placement_group}"
}

output "ec2_key_name" {
  value = "${module.jenkins_ec2.key_name}"
}

output "ec2_password_data" {
  value       = "${module.jenkins_ec2.password_data}"
  description = "Base-64 encoded encrypted password data for the instance. Useful for getting the administrator password for instances running Microsoft Windows. attribute is only exported if get_password_data is true. Note that encrypted value will be stored in the state file, as with all exported attributes. See GetPasswordData for more information."
}

output "ec2_public_dns" {
  value = "${module.jenkins_ec2.public_dns}"
}

output "ec2_public_ip" {
  value = "${module.jenkins_ec2.public_ip}"
}

output "ec2_ipv6_addresses" {
  value = "${module.jenkins_ec2.ipv6_addresses}"
}

output "ec2_primary_network_interface_id" {
  value = "${module.jenkins_ec2.primary_network_interface_id}"
}

output "ec2_private_dns" {
  value = "${module.jenkins_ec2.private_dns}"
}

output "ec2_private_ip" {
  value = "${module.jenkins_ec2.private_ip}"
}

output "ec2_security_groups" {
  value = "${module.jenkins_ec2.security_groups}"
}

output "ec2_vpc_security_group_ids" {
  value = "${module.jenkins_ec2.vpc_security_group_ids}"
}

output "ec2_subnet_id" {
  value = "${module.jenkins_ec2.subnet_id}"
}

########################################
# JENKINS EFS
########################################
output "jenkins_efs_id" {
  value = "${module.jenkins_efs.efs_id}"
}

output "jenkins_efs_arn" {
  value = "${module.jenkins_efs.efs_arn}"
}

output "jenkins_efs_dns_name" {
  value = "${module.jenkins_efs.efs_dns_name}"
}

output "jenkins_efs_mount_target_id" {
  value = "${module.jenkins_efs.efs_mount_target_id}"
}

output "jenkins_efs_mount_target_dns_name" {
  value = "${module.jenkins_efs.efs_mount_target_dns_name}"
}

output "jenkins_efs_mount_target_network_interface_id" {
  value = "${module.jenkins_efs.efs_mount_target_network_interface_id}"
}

output "jenkins_efs_mount_target_security_group_id" {
  value = "${module.jenkins_efs.efs_mount_target_security_group_id}"
}

output "jenkins_efs_mount_target_security_group_arn" {
  value = "${module.jenkins_efs.efs_mount_target_security_group_arn}"
}

output "jenkins_efs_mount_target_security_group_vpc_id" {
  value = "${module.jenkins_efs.efs_mount_target_security_group_vpc_id}"
}

output "jenkins_efs_mount_target_security_group_name" {
  value = "${module.jenkins_efs.efs_mount_target_security_group_name}"
}

output "jenkins_efs_mount_target_security_group_ingress" {
  value = "${module.jenkins_efs.efs_mount_target_security_group_ingress}"
}

output "jenkins_efs_mount_target_security_group_egress" {
  value = "${module.jenkins_efs.efs_mount_target_security_group_egress}"
}
