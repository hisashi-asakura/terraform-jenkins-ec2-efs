## Get Started
1. Set AWS credential variables for Terraform (sometimes Terraform doesn't use `~/.aws/credentials`, `~/.aws/config`, nor `AWS_DEFAULT_PROFILE`):

`export AWS_ACCESS_KEY_ID=YOUR_ACCESS_KEY_HERE`

`export AWS_SECRET_ACCESS_KEY=YOUR_SECRET_ACCESS_KEY_HERE`

2. Change directory to `/composition/../dev`

3. Execute Terraform commands:

`terraform init`

`terraform plan`

`terraform apply`

`terraform destroy`


## TODO:

### incorporate best practice project structure 
- composition
  - what variable values here??
    - region?
    - cidr block
    - NO PROVIDER

- infra
  - EC2-jenkins module
    - data module
      - instance type
      - cidr block
      - data template for userdata
      - SG rules 
      
- resource
  - EC2
  - efs
  - SG